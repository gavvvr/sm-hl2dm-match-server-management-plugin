#pragma semicolon 1

#include <sourcemod>
#include <steamtools>
#include <smlib>
#include <morecolors>
#undef REQUIRE_PLUGIN
#include <updater>

#define UPDATE_URL "http://bitbucket.org/gavvvr/sm-hl2dm-match-server-management-plugin/raw/master/hl2dm-pms-servermanagement.txt"

#define MAX_SAYTXT_LENGTH 192
#define MAX_IP_LENGTH 17
#define MAX_IP_PORT_LENGTH 23
#define MAX_MAPNAME_LENGTH	32

new svPureChar = '2';

new bool:stvwarnShownOnce = false;
new bool:cmUnavailible = false;
new bool:startUnavailible = false;
new bool:cfinprogress = false;
new bool:matchIsON = false; // since all plugins are being reloaded on match start, the plugin can't know the match was started. This value is based on sv_allow_wait_command 0 change, which is used in countdown cfgs.

new cfClUID;
new const String:coinside[][] = {"heads", "tails"};

new Handle:g_Cvar_TeamPlay = INVALID_HANDLE;
new Handle:g_Cvar_PW = INVALID_HANDLE;
new Handle:g_Cvar_AllowWait = INVALID_HANDLE;
new Handle:g_Cvar_TVEnabled = INVALID_HANDLE;
new Handle:g_Cvar_TVPort = INVALID_HANDLE;
new Handle:g_Cvar_TVDelay = INVALID_HANDLE;
new Handle:g_Cvar_SvCheats = INVALID_HANDLE;

new Handle:g_hPwLen = INVALID_HANDLE;
new Handle:g_hAdmReq = INVALID_HANDLE;
new Handle:g_hPrimPref = INVALID_HANDLE;
new Handle:g_hSecPref = INVALID_HANDLE;
new Handle:g_hRRMapOnTpChange = INVALID_HANDLE;
new Handle:g_hStartCooldown = INVALID_HANDLE;
new Handle:g_hDemosPath = INVALID_HANDLE;

new String:demopath[PLATFORM_MAX_PATH];
new String:pmskvfilepath[PLATFORM_MAX_PATH];
new String:primPref[10];
new String:secPref[10];

public Plugin:myinfo =
{
	name = "HL2DM Match Plugin",
	author = "gavvvr, hl2dm.net",
	description = "Server management plugin for hl2dm match servers",
	version = "0.10.0",
	url = "http://hl2dm.net/"
};

public APLRes:AskPluginLoad2(Handle:myself, bool:late, String:error[], err_max)
{
	// Updater
	MarkNativeAsOptional("Updater_AddPlugin");
	MarkNativeAsOptional("ReloadPlugin");
	
	return APLRes_Success;
}

public OnPluginStart()
{
	HookUserMessage(GetUserMessageId("VGUIMenu"), Hook_VGUIMenu);

	HookEvent("round_start", OnRoundStart);

	RegConsoleCmd("cm", Command_ChangeMap, "cm <mapname>");
	RegConsoleCmd("start", Command_StartMatch, "start <cfg shortcut>");
	RegConsoleCmd("tp", Command_Teamplay, "tp <0|1>");
	RegConsoleCmd("teamplay", Command_Teamplay, "teamplay <0|1>");
	RegConsoleCmd("pw", Command_Password, "pw - displays the current passowrd");
	RegConsoleCmd("stv", Command_STV, "stv - offers to connect to sourceTV server");
	RegConsoleCmd("sm_password", Command_Password, "password - displays the current passowrd");
	RegConsoleCmd("coinflip", Command_Coinflip, "coinflip - let a random Player flip the coin");
	RegConsoleCmd("cf", Command_Coinflip, "cf - let a random Player flip the coin");
	RegConsoleCmd("?", Command_Help, "help - displays the matchserver's help");
	RegConsoleCmd("h", Command_Help, "help - displays the matchserver's help");
	RegConsoleCmd("hlp", Command_Help, "help - displays the matchserver's help");
	RegConsoleCmd("st", Command_Status, "st - displays the status in player's console");
	RegConsoleCmd("sm_status", Command_Status, "sm_status - displays the status in player's console");
	RegConsoleCmd("mp", Command_MapPool, "mp <1v1/2v2/3v3> - displays the mappool");

	g_Cvar_TeamPlay = FindConVar("mp_teamplay");
	g_Cvar_PW = FindConVar("sv_password");
	g_Cvar_AllowWait = FindConVar("sv_allow_wait_command");
	g_Cvar_SvCheats = FindConVar("sv_cheats");

	HookConVarChange(g_Cvar_SvCheats, ConVarChange_Cheats);
	HookConVarChange(g_Cvar_AllowWait, ConVarChange_AllowWaitCmd);
	AddCommandListener(OnSvPureExecuted, "sv_pure");
	AddCommandListener(Command_Jointeam, "jointeam");

	g_Cvar_TVEnabled = FindConVar("tv_enable");
	g_Cvar_TVPort = FindConVar("tv_port");
	g_Cvar_TVDelay = FindConVar("tv_delay");

	g_hPwLen = CreateConVar("sm_servmanagement_pwlen", "4", "Defines the password length. If 0 - password won't be set", _, true, 0.0, true, 10.0);
	g_hAdmReq = CreateConVar("sm_servmanagement_admin_required", "0", "Defines if the admin rights required to execute server management commands", _, true, 0.0, true, 1.0);
	g_hPrimPref = CreateConVar("sm_servmanagement_prefix_primary", "!", "Sets the primary chat command prefix.");
	g_hSecPref = CreateConVar("sm_servmanagement_prefix_secondary", "#.#", "Sets the secondary chat command prefix.");
	g_hRRMapOnTpChange = CreateConVar("sm_rrontp", "0", "Defines if the current map will be reloaded immediatately on teamplay mode change so the changes take an effect", _, true, 0.0, true, 1.0);
	g_hStartCooldown = CreateConVar("sm_start_cooldown", "20", "Time in seconds while cfg execution is unavailible", _, true, 0.0, false, _);
	g_hDemosPath = CreateConVar("sm_demospath", "./demos", "Sets the directory, where the *.dem files will be stored. The specified directory should be created manually");

	if (LibraryExists("updater"))
	{
		Updater_AddPlugin(UPDATE_URL);
	}

	AutoExecConfig();
}

public OnConfigsExecuted()
{
	GetConVarString(g_hPrimPref, primPref, sizeof(primPref));
	StrCat(primPref, sizeof(primPref), " ");
	GetConVarString(g_hSecPref, secPref, sizeof(secPref));
	StrCat(secPref, sizeof(secPref), " ");

	BuildPath(Path_SM, pmskvfilepath, sizeof(pmskvfilepath), "configs/pms.cfg");
	if (!FileExists(pmskvfilepath))
	{
		LogError("The file configs/pms.cfg does not exist");
		SetFailState("configs/pms.cfg not found");
	}

	GetConVarString(g_hDemosPath, demopath, sizeof(demopath));
	if(!DirExists(demopath))
	{
		CreateDirectory(demopath, 0755);
		if(!DirExists(demopath))
		{
			LogError("Demos folder does not exist and can't be created");
			SetFailState("Demos folder does not exist and can't be created");
		}
	}
}

public Action:Event_PlayerSpawnOnCountdown(Handle:event, const String:name[], bool:dontBroadcast)
{
	new client = GetClientOfUserId(GetEventInt(event, "userid"));
	SetEntityMoveType(client, MOVETYPE_NONE);
	CreateTimer(0.1, Timer_RemoveAllWeps, GetClientUserId(client), TIMER_FLAG_NO_MAPCHANGE); //made this timer, because immidiate Client_RemoveAllWeapons(client) does not work
}

public Action:Timer_RemoveAllWeps(Handle:timer, any:usrID)
{
	new client = GetClientOfUserId(usrID);
	if (client == 0 || !GetConVarBool(g_Cvar_AllowWait)) //invalid userID or the game was started
	{
		return Plugin_Continue;
	}
	Client_RemoveAllWeapons(client);
	return Plugin_Continue;
}

public Action:OnRoundStart(Handle:event, const String:name[], bool:dontBroadcast)
{
	CheckSvCheats(GetConVarInt(g_Cvar_SvCheats), true);
	if (GetConVarBool(g_Cvar_AllowWait))
	{
		FreezeAll();
	}
}

FreezeAll()
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i) && !IsClientObserver(i))
		{
			SetEntityMoveType(i, MOVETYPE_NONE);
			Client_RemoveAllWeapons(i);
		}
	}
}

public OnLibraryAdded(const String:name[])
{
    if (StrEqual(name, "updater"))
    {
        Updater_AddPlugin(UPDATE_URL);
    }
}

public OnClientDisconnect(client)
{
	if (client == GetClientOfUserId(cfClUID))
	{
		cfinprogress = false;
	}
}

public OnMapStart()
{
	CreateTimer(0.1, parseSvPureOnMapStart);
	if (GetConVarBool(g_Cvar_AllowWait))
	{
		HookEvent("player_spawn", Event_PlayerSpawnOnCountdown, EventHookMode_Post);
	}
}

public OnMapEnd()
{
	stvwarnShownOnce = false;
	matchIsON = false;
}

public Action:parseSvPureOnMapStart(Handle:timer)
{
	ParseSvPure();
}

public ConVarChange_AllowWaitCmd(Handle:convar, const String:oldValue[], const String:newValue[])
{
	if (StrEqual(newValue, "0"))
	{
		matchIsON = true;

		UnhookEvent("player_spawn", Event_PlayerSpawnOnCountdown, EventHookMode_Post);

		//Printing pw to chat except spectators.
		new String:servpw[20];
		GetConVarString(g_Cvar_PW, servpw, sizeof(servpw));
		for (new x = 1; x <= MaxClients; x++)
		{
			if (IsClientInGame(x) && !IsFakeClient(x))
			{
				if (IsClientObserver(x))
				{
					CPrintToChat(x, "{aliceblue}Password - {purple}!pw");
				}
				else
				{
					CPrintToChat(x, "{aliceblue}Password was set to: {purple}%s", servpw);
				}
			}
		}

		//Prinring status string of every player to STV chat and STV-demo
		for (new i = 1; i <= MaxClients; i++)
		{
			if (IsClientInGame(i))
			{
				decl String:clStatusStr[90];
				GetPlayerStatusString(i, clStatusStr, sizeof(clStatusStr));
				LogToGame("%s", clStatusStr);
				PrintToChat(GetStvIndex(), "%s", clStatusStr);
			}
		}
	}
	if (StrEqual(newValue, "1"))
	{
		HookEvent("player_spawn", Event_PlayerSpawnOnCountdown, EventHookMode_Post);
	}
}

public ConVarChange_Cheats(Handle:convar, const String:oldValue[], const String:newValue[])
{
	CheckSvCheats(StringToInt(newValue), matchIsON);
}

CheckSvCheats(intVal, bool:isMatchOn)
{
	if (isMatchOn && intVal != 0)
	{
		CPrintToChatAll("[SM] {crimson}Warning! {default}sv_cheats was set to 1");
	}
}

public Action:OnSvPureExecuted(client, const String:command[], args)
{
	if (client != 0 || args < 1)
	{
		return Plugin_Continue;
	}
	if (matchIsON)
	{
		new String:svPurestr[2];
		GetCmdArgString(svPurestr, sizeof(svPurestr));
		CheckSvPure(StringToInt(svPurestr), true);
	}
	return Plugin_Continue;
}

CheckSvPure(intVal, bool:isMatchOn)
{
	if (isMatchOn && intVal != 2 && intVal != 1)
	{
		CPrintToChatAll("[SM] {crimson}Warning! {default}sv_pure has wrong value");
	}
}

ParseSvPure()
{
	decl String:svPureConOut[600];
	ServerCommandEx(svPureConOut, sizeof(svPureConOut), "sv_pure");
	decl startindex;
	startindex = StrContains(svPureConOut, "Current sv_pure value is", true);
	if (startindex != -1)
	{
		svPureChar = svPureConOut[startindex+25];
	}
	else
	{
		LogMessage("Could not parse sv_pure output");
	}
}

public Action:Command_Jointeam(client, const String:command[], args)
{
	if (!GetConVarBool(g_Cvar_AllowWait))
	{
		return Plugin_Continue;
	}
	if (args < 1)
	{
		return Plugin_Continue;
	}
	new String:jointeamArg[2];
	GetCmdArgString(jointeamArg, sizeof(jointeamArg));
	if (StrEqual(jointeamArg, "2") || StrEqual(jointeamArg, "3"))
	{
		new String:clientTeam[2];
		IntToString(GetClientTeam(client), clientTeam, sizeof(clientTeam));
		if (StrEqual(clientTeam, jointeamArg) || GetClientTeam(client) == 0) // rebel or combine tries to join the team in which he already is (to get unfreezed), or player in dm-more tries to get unfreezed.
		{
			return Plugin_Handled;
		}
	}
	return Plugin_Continue;
}

public Action:Hook_VGUIMenu(UserMsg:msg_id, Handle:msg, const players[], playersNum, bool:reliable, bool:init)
{
	new String:s[10];
	BfReadString(msg, s, sizeof(s));
	if (strcmp(s, "scores", false) == 0 && !stvwarnShownOnce && matchIsON)
	{
		CreateTimer(0.1, broadcastWarning);
		stvwarnShownOnce = true;
		return Plugin_Continue;
	}
	return Plugin_Continue;
}

public Action:broadcastWarning(Handle:timer)
{
	CPrintToChatAll("[SM] {crimson}Warning! {default}Map is over. Don't change map or pause the game if the match is being broadcasted via SourceTV!");
}

public Action:OnClientSayCommand(client, const String:command[], const String:sArgs[])
{
	if (cfinprogress)
	{
		decl String:guess[MAX_SAYTXT_LENGTH];
		strcopy(guess, sizeof(guess), sArgs);
		StripQuotes(guess);
		new actual = GetRandomInt(0, 1);
		if (StrEqual(guess, coinside[actual], false)) //won
		{
			return AnswerOnGuess(client, guess, actual, true);
		}
		else if (StrEqual(guess, coinside[Math_Abs(actual-1)], false)) //lost
		{
			return AnswerOnGuess(client, guess, actual, false);
		}
		if (client == GetClientOfUserId(cfClUID))
		{
			CPrintToChat(client,"{aliceblue}Pick {purple}heads {aliceblue}or {purple}tails{aliceblue}.");
			return Plugin_Continue;
		}
	}
	new startidx;
	if (sArgs[0] == '"')
	{
		startidx = 1;
	}
	if((strncmp(sArgs[startidx], primPref, strlen(primPref), true) == 0) || (strncmp(sArgs[startidx], secPref, strlen(primPref), true) == 0))
	{
		decl String:txtArg[MAX_SAYTXT_LENGTH];
		new String:explodedArgStr[3][20];
		strcopy(txtArg, sizeof(txtArg), sArgs);
		StripQuotes(txtArg);
		ExplodeString(txtArg, " ", explodedArgStr, sizeof(explodedArgStr), sizeof(explodedArgStr[]));
		if (StrEqual(explodedArgStr[1], "cm", false))
		{
			FakeClientCommand(client, "say /cm %s", explodedArgStr[2]);
		}
		else if (StrEqual(explodedArgStr[1], "start", false))
		{
			FakeClientCommand(client, "say /start %s", explodedArgStr[2]);
		}
		else if (StrEqual(explodedArgStr[1], "teamplay", false) || StrEqual(explodedArgStr[1], "tp", false))
		{
			FakeClientCommand(client, "say /tp %s", explodedArgStr[2]);
		}
		else if (StrEqual(explodedArgStr[1], "pw", false) || StrEqual(explodedArgStr[1], "password", false) || StrEqual(explodedArgStr[1], "pass", false))
		{
			FakeClientCommand(client, "say /pw");
		}
		else if (StrEqual(explodedArgStr[1], "stv", false))
		{
			FakeClientCommand(client, "say /stv");
		}
		else if (StrEqual(explodedArgStr[1], "coinflip", false) || StrEqual(explodedArgStr[1], "cf", false))
		{
			FakeClientCommand(client, "say /coinflip");
		}
		else if (StrEqual(explodedArgStr[1], "help", false) || StrEqual(explodedArgStr[1], "hlp", false) || StrEqual(explodedArgStr[1], "h", false) || StrEqual(explodedArgStr[1], "?", false))
		{
			FakeClientCommand(client, "say /h");
		}
		else if (StrEqual(explodedArgStr[1], "st", false) || StrEqual(explodedArgStr[1], "status", false))
		{
			FakeClientCommand(client, "say /sm_status");
		}
		else if (StrEqual(explodedArgStr[1], "mp", false))
		{
			FakeClientCommand(client, "say /mp %s", explodedArgStr[2]);
		}
		else
		{
			CPrintToChatAll("{aliceblue}[SM] Command not found, use {purple}%shelp{aliceblue} to get the list of availible commands", primPref);
		}
		return Plugin_Continue;
	}
	else
	{
		return Plugin_Continue;
	}
}

public Action:Command_ChangeMap(client, args)
{
	if (!IsConCmdAccessable(client, false, true))
	{
		return Plugin_Handled;
	}
	if(cmUnavailible)
	{
		if (client == 0 && GetCmdReplySource() == SM_REPLY_TO_CONSOLE)
		{
			PrintToServer("Someone already initialized map change");
			return Plugin_Handled;
		}
		CPrintToChat(client,"{aliceblue}Someone already initialized map change");
		return Plugin_Handled;
	}
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: cm <map>");
		return Plugin_Handled;
	}
	decl String:mapArg[MAX_MAPNAME_LENGTH];
	new String:mapName[MAX_MAPNAME_LENGTH];
	GetCmdArg(1, mapArg, sizeof(mapArg));
	new Handle:f_pmskv = CreateKeyValues("");
	if (!FileToKeyValues(f_pmskv, pmskvfilepath))
	{
		if (client == 0 && GetCmdReplySource() == SM_REPLY_TO_CONSOLE)
		{
			PrintToServer("Failed to read key/values from pms.cfg");
			return Plugin_Handled;
		}
		CPrintToChat(client,"{aliceblue}Failed to read key/values from pms.cfg");
		return Plugin_Handled;
	}
	if (KvJumpToKey(f_pmskv, "Maps"))
		KvGetString(f_pmskv, mapArg, mapName, sizeof(mapName));
	CloseHandle(f_pmskv);
	if(!IsMapValid(mapName))
	{
		if (client == 0 && GetCmdReplySource() == SM_REPLY_TO_CONSOLE)
		{
			PrintToServer("Map %s is not available on this Server.", mapArg);
			return Plugin_Handled;
		}
		CPrintToChat(client,"{aliceblue}Map {purple}%s {aliceblue}is not available on this Server.", mapArg);
		return Plugin_Handled;
	}
	cmUnavailible = true;
	if (client == 0 && GetCmdReplySource() == SM_REPLY_TO_CONSOLE)
		{
			PrintToServer("Map will change to %s in 5 seconds.", mapName);
		}
		else
		{
			CPrintToChatAll("{aliceblue}Map will change to {purple}%s {aliceblue}in 5 seconds ({purple}%N{aliceblue})", mapName, client);
		}
	new Handle:mapNamePack = CreateDataPack();
	WritePackString(mapNamePack, mapName);
	CreateTimer(5.0, changeMapIn5secs, mapNamePack);
	return Plugin_Continue;
}

public Action:changeMapIn5secs(Handle:timer, Handle:pack)
{
	decl String:mapName[MAX_MAPNAME_LENGTH];
	ResetPack(pack);
	ReadPackString(pack, mapName, sizeof(mapName));
	ServerCommand("changelevel %s", mapName);
	cmUnavailible = false;
}


public Action:Command_StartMatch(client, args)
{
	if (!IsConCmdAccessable(client, false, true))
	{
		return Plugin_Handled;
	}
	if (startUnavailible || (GetTime() - GetFileTime(demopath, FileTime_LastChange)) < GetConVarInt(g_hStartCooldown))
	{
		if (client == 0 && GetCmdReplySource() == SM_REPLY_TO_CONSOLE)
		{
			PrintToServer("Wait %d seconds after match start to be able to execute config again!", GetConVarInt(g_hStartCooldown));
			return Plugin_Handled;
		}
		CPrintToChat(client,"{aliceblue}Wait %d seconds after match start to be able to execute config again!", GetConVarInt(g_hStartCooldown));
		return Plugin_Handled;
	}
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: start <cfg>");
		return Plugin_Handled;
	}
	new String:cfgName[100];
	new String:cfgArg[15];
	GetCmdArg(1, cfgArg, sizeof(cfgArg));
	new Handle:f_pmskv = CreateKeyValues("");
	if (!FileToKeyValues(f_pmskv, pmskvfilepath))
	{
		if (client == 0 && GetCmdReplySource() == SM_REPLY_TO_CONSOLE)
		{
			PrintToServer("Failed to read key/values from pms.cfg");
			return Plugin_Handled;
		}
		CPrintToChat(client,"{aliceblue}Failed to read key/values from pms.cfg");
		return Plugin_Handled;
	}
	if (KvJumpToKey(f_pmskv, "Configs"))
		KvGetString(f_pmskv, cfgArg, cfgName, sizeof(cfgName));
	CloseHandle(f_pmskv);
	if(!FileExists(cfgName))
	{
		if (client == 0 && GetCmdReplySource() == SM_REPLY_TO_CONSOLE)
		{
			PrintToServer("Config not found! %s", cfgArg);
			return Plugin_Handled;
		}
		CPrintToChat(client,"{aliceblue}Config not found! {purple}%s", cfgArg);
		return Plugin_Handled;
	}
	new String:cfgStamp[20];
	File_GetFileName(cfgName, cfgStamp, sizeof(cfgStamp));
	if (client != 0)
	{
		CPrintToChatAll("{purple}%N {aliceblue}executed {purple}%s.cfg", client, cfgStamp);
	}
	else
	{
		CPrintToChatAll("{purple}Console {aliceblue}executed {purple}%s.cfg", cfgStamp);
	}
	decl String:demtimestamp[19];
	decl String:mapstamp[MAX_MAPNAME_LENGTH];
	FormatTime(demtimestamp, sizeof(demtimestamp), "%Y%m%d-%Hh%Mm%Ss");
	GetCurrentMap(mapstamp, sizeof(mapstamp));
	ServerCommand("tv_stoprecord");
	ServerCommand("tv_record %s/%s-%s-%s", demopath, demtimestamp, mapstamp, cfgStamp);
	FreezeAll();
	SetConVarBool(g_Cvar_AllowWait, true, false, true);
	if (svPureChar != '1' && svPureChar != '2')
	{
		CPrintToChatAll("[SM] {crimson}Warning! {default}sv_pure had wrong value before match start");
		CPrintToChatAll("[SM] You probably need to add {crimson}\"-sv_pure\" {default}launch option for your server");
	}
	
	decl String:statusStr[1000];
	GetCompleteStatusString(statusStr, sizeof(statusStr));
	for (new i = 1; i <= MaxClients; i++)
	{
		if(IsClientInGame(i) && !IsFakeClient(i))
		{
			PrintToConsole(i, "%s", statusStr);
		}
	}

	SetSvPassword();

	startUnavailible = true; //useless ATM because all plugins are realoaded on match start
	CreateTimer(GetConVarFloat(g_hStartCooldown), turnOnStartCmd);

	new Handle:cfgNamePack = CreateDataPack();
	WritePackString(cfgNamePack, cfgName[4]);
	CreateTimer(0.1, execcfg, cfgNamePack);
	// ServerCommand("exec %s", cfgName[4]); //has problems when using command from server's console (no countdown)
	return Plugin_Handled;
}

public Action:turnOnStartCmd(Handle:timer)
{
	startUnavailible = false;
}

public Action:execcfg(Handle:timer, Handle:pack)
{
	new String:cfgName[50];
	ResetPack(pack);
	ReadPackString(pack, cfgName, sizeof(cfgName));
	ServerCommand("exec %s", cfgName);

	CheckSvCheats(GetConVarInt(g_Cvar_SvCheats), true);
}

public Action:Command_Teamplay(client, args)
{
	if (!IsConCmdAccessable(client, false, true))
	{
		return Plugin_Handled;
	}
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: teamplay <0/1>");
		return Plugin_Handled;
	}
	decl String:tpval[5];
	new bool:oldVal = GetConVarBool(g_Cvar_TeamPlay);
	GetCmdArg(1, tpval, sizeof(tpval));
	if (StrEqual(tpval, "1", false) || StrEqual(tpval, "on", false))
	{
		SetConVarBool(g_Cvar_TeamPlay, true, false, true);
		if (!GetConVarBool(g_hRRMapOnTpChange))
		{
			if (!oldVal)
			{
				CPrintToChatAll("{purple}TDM-mode {aliceblue}activated after mapchange ({purple}%N{aliceblue})", client);
			}
			return Plugin_Handled;
		}
		RestartMap();
		return Plugin_Handled;
	}
	else if (StrEqual(tpval, "0", false) || StrEqual(tpval, "off", false))
	{
		SetConVarBool(g_Cvar_TeamPlay, false, false, true);
		if (!GetConVarBool(g_hRRMapOnTpChange))
		{
			if (oldVal)
			{
			CPrintToChatAll("{purple}Deathmatch-mode {aliceblue}activated after mapchange ({purple}%N{aliceblue})", client);
			}
			return Plugin_Handled;
		}
		RestartMap();
		return Plugin_Handled;
	}
	ReplyToCommand(client, "[SM] Usage: teamplay <0/1>");
	return Plugin_Handled;
}

RestartMap()
{
	decl String:mapName[MAX_MAPNAME_LENGTH];
	GetCurrentMap(mapName, sizeof(mapName));
	ServerCommand("changelevel %s", mapName);
}

public Action:Command_Password(client, args)
{
	if (!IsConCmdAccessable(client, false, true))
	{
		return Plugin_Handled;
	}
	decl String:servpw[20];
	GetConVarString(g_Cvar_PW, servpw, sizeof(servpw));
	if(StrEqual(servpw, "", true))
	{
		if (client == 0 && GetCmdReplySource() == SM_REPLY_TO_CONSOLE)
		{
			PrintToServer("Server has no Password.");
			return Plugin_Handled;
		}
		CPrintToChat(client,"{aliceblue}Server has no Password.");
		return Plugin_Handled;
	}
	if (client == 0 && GetCmdReplySource() == SM_REPLY_TO_CONSOLE)
	{
		PrintToServer("Password: %s", servpw);
		return Plugin_Handled;
	}
	CPrintToChat(client,"{aliceblue}Password: {purple}%s", servpw);
	return Plugin_Handled;
}

public Action:Command_STV(client, args)
{
	if (!IsConCmdAccessable(client, true, false))
	{
		return Plugin_Handled;
	}
	if (GetConVarBool(g_Cvar_TVEnabled))
	{
		if (STVBotIsPresent())
		{
			new String:clientIP[MAX_IP_LENGTH];
			GetClientIP(client, clientIP, sizeof(clientIP));
			if (IsIPLocal(IPToLong(clientIP)))
			{
				return TryToDisplayConnectBox(client, false);
			}
			return TryToDisplayConnectBox(client, true);
		}
		ReplyToCommand(client, "SourceTV is enabled, but the STV bot is not found on this server");
	}
	ReplyToCommand(client, "[SM] SourceTV is not availible on this server");
	return Plugin_Handled;
}

Action:TryToDisplayConnectBox(client, bool:publicClient)
{
	new String:IPdottedString[MAX_IP_PORT_LENGTH];
	if (Server_GetIPString(IPdottedString, sizeof(IPdottedString), publicClient))
	{
		Format(String:IPdottedString, sizeof(IPdottedString), "%s:%d", IPdottedString, GetConVarInt(g_Cvar_TVPort));
		DisplayAskConnectBox(client, 10.0, IPdottedString);
		return Plugin_Handled;
	}
	ReplyToCommand(client, "Failed to get server's IP");
	return Plugin_Handled;
}

public Action:Command_Coinflip(client, args)
{
	if (!IsConCmdAccessable(client, true, false))
	{
		return Plugin_Handled;
	}
	if (cfinprogress)
	{
		CPrintToChat(client, "{aliceblue}$N already initialized coinflip, now he should pick {purple}heads{aliceblue} or {purple}tails.", GetClientOfUserId(cfClUID));
	}
	cfClUID = GetClientUserId(client);
	CPrintToChatAll("{aliceblue}Player {purple}%N {aliceblue}has to flip the coin.", client);
	CPrintToChat(client,"{aliceblue}Pick {purple}heads {aliceblue}or {purple}tails{aliceblue}.");
	cfinprogress = true;
	CreateTimer(20.0, finishCoinflip);
	return Plugin_Handled;
}

public Action:finishCoinflip(Handle:timer)
{
	cfinprogress = false;
}

Action:AnswerOnGuess(client, String:guess[], actual, bool:win)
{
	if (GetClientUserId(client) != cfClUID)
	{
		CPrintToChat(client, "%N should flip the coin, not you.", GetClientOfUserId(cfClUID));
		return Plugin_Handled;
	}
	if (win)
	{
		CPrintToChatAll("{aliceblue}Guess: {purple}%s {aliceblue}The Coin shows: {purple}%s", guess, coinside[actual]);
		CPrintToChatAll("{green}%N won the coinflip.", GetClientOfUserId(cfClUID));
	}
	else
	{
		CPrintToChatAll("{aliceblue}Guess: {purple}%s {aliceblue}The Coin shows: {purple}%s", guess, coinside[actual]);
		CPrintToChatAll("{crimson}%N lost the coinflip.", GetClientOfUserId(cfClUID));
	}
	cfinprogress = false;
	return Plugin_Continue;
}

public Action:Command_Help(client, args)
{
	if (!IsConCmdAccessable(client, true, false))
	{
		return Plugin_Handled;
	}
	CPrintToChat(client, "{aliceblue}Every command can be triggered from chat like: %s cm, %s cm, !cm and /cm,", primPref, secPref);
	CPrintToChat(client, "{aliceblue}or from the console: cm");
	CPrintToChat(client, "{aliceblue}teamplay or tp <1/0> to switch Teamplay-Mode ON and OFF");
	CPrintToChat(client, "{aliceblue}start <cfg> to start a Match");
	CPrintToChat(client, "{aliceblue}cm <mapname> to change the Map");
	CPrintToChat(client, "{aliceblue}password or pw to see the current password");
	CPrintToChat(client, "{aliceblue}coinflip to let a random Player flip the coin");
	CPrintToChat(client, "{aliceblue}stv to join stv server");
	CPrintToChat(client, "{aliceblue}mp <1v1/2v2/3v3> to the current map pool");
	return Plugin_Handled;
}

public Action:Command_Status(client, args)
{
	if (GetCmdReplySource() == SM_REPLY_TO_CHAT)
	{
		ReplyToCommand(client, "[SM] See console for output");
	}
	decl String:statusStr[1000];
	GetCompleteStatusString(statusStr, sizeof(statusStr));
	PrintToConsole(client, "%s", statusStr);
	return Plugin_Handled;
}

public Action:Command_MapPool(client, args)
{
	if (args < 1)
	{
		ReplyToCommand(client, "[SM] Usage: mp <1v1/2v2/3v3>");
		return Plugin_Handled;
	}

	decl String:mappoolArg[10];
	new String:mappoolFileName[10];
	GetCmdArg(1, mappoolArg, sizeof(mappoolArg));
	new Handle:f_pmskv = CreateKeyValues("");
	if (!FileToKeyValues(f_pmskv, pmskvfilepath))
	{
		ReplyToCommand(client,"Failed to read key/values from pms.cfg");
		return Plugin_Handled;
	}
	if (KvJumpToKey(f_pmskv, "Mappools"))
		KvGetString(f_pmskv, mappoolArg, mappoolFileName, sizeof(mappoolFileName));
	CloseHandle(f_pmskv);

	new String:mappoolPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, mappoolPath, sizeof(mappoolPath), "data/mappool/%s.txt", mappoolFileName);
	if (!FileExists(mappoolPath))
	{
		if (client != 0)
		{
			CPrintToChat(client, "{purple}%s.txt {aliceblue}not found.", mappoolFileName);
		}
		else
		{
			ReplyToCommand(client, "%s.txt not found.", mappoolFileName);
		}
		return Plugin_Handled;
	}
	new Handle:fileMapPool = OpenFile(mappoolPath, "r");
	new String:mapPoolStr[MAX_MAPNAME_LENGTH];
	if(fileMapPool != INVALID_HANDLE)
	{
		while(!IsEndOfFile(fileMapPool) && ReadFileLine(fileMapPool, mapPoolStr, sizeof(mapPoolStr)))
		{
			TrimString(mapPoolStr);
			ReplyToCommand(client, mapPoolStr);
		}
	}
	return Plugin_Handled;
}

GetCompleteStatusString(String:status[], size)
{
	new String:hName[MAX_NAME_LENGTH],
	String:localIP[MAX_IP_LENGTH], 
	String:pubIP[MAX_IP_LENGTH], 
	String:mapName[MAX_MAPNAME_LENGTH];
	new svPort = Server_GetPort();

	Server_GetHostName(hName, sizeof(hName));
	Server_GetIPString(localIP, sizeof(localIP), false);
	Server_GetIPString(pubIP, sizeof(pubIP), true);
	GetCurrentMap(mapName, sizeof(mapName));

	Format(status, size, "hostname: %s\nudp/ip: %s:%d  (public ip: %s:%d)\nmap: %s\nsourcetv: port %d, delay %.1fs\nplayers: %d (%d max)", hName, localIP, svPort, pubIP, svPort, mapName, GetConVarInt(g_Cvar_TVPort), GetConVarFloat(g_Cvar_TVDelay), Client_GetCount(true, true), MaxClients);

	for (new i = 1; i <= MaxClients; i++)
	{
		if (IsClientInGame(i))
		{
			decl String:clStatusStr[90];
			GetPlayerStatusString(i, clStatusStr, sizeof(clStatusStr));
			StrCat(status, size, clStatusStr);
		}
	}
}

GetPlayerStatusString(client, String:playerStatus[], size)
{
	new String:clAuthID[MAX_STEAMAUTH_LENGTH], 
	String:clIP[MAX_IP_LENGTH], 
	clLatency, 
	clALatency, 
	clALoss;

	if (IsFakeClient(client))
	{
		clAuthID = "BOT";
		clIP = "localhost";
		clLatency = 0;
		clALatency = 0;
		clALoss = 0;
	}
	else
	{
		if (!IsClientAuthorized(client))
		{
			clAuthID = "Not Authorized";
		}
		else
		{
			if (!GetClientAuthString(client, clAuthID, sizeof(clAuthID)))
			{
				clAuthID = "n/a";
			}
		}
		if (!GetClientIP(client, clIP, sizeof(clIP)))
		{
			clIP = "n/a";
		}
		clLatency = RoundToNearest(GetClientLatency(client, NetFlow_Both)*1000);
		clALatency = RoundToNearest(GetClientAvgLatency(client, NetFlow_Both)*1000);
		clALoss = RoundToNearest(GetClientAvgLoss(client, NetFlow_Both)*1000);
	}
	Format(playerStatus, size, "\n#\t%N\t(%s)\t%s\tPing: %d ms(Avg: %d ms)\t Avg.loss: %d", client, clAuthID, clIP, clLatency, clALatency, clALoss);
}

bool:IsConCmdAccessable(client, bool:accessableForAllPlayers, bool:accessableForSvConsole)
{
	if (client == 0)
	{
		if (accessableForSvConsole)
		{
			return true;
		}
		ReplyToCommand(client, "[SM] Command is in-game only");
		return false;
	}
	else if (!GetConVarBool(g_hAdmReq) || (GetUserFlagBits(client) != 0) || accessableForAllPlayers)
	{
		return true;
	}
	ReplyToCommand(client, "[SM] You don't have access to this command");
	return false;
}

SetSvPassword()
{
	if (!GetConVarBool(g_hPwLen))
	{
	}
	else
	{
		new pwstrlen = RoundToCeil(GetConVarFloat(g_hPwLen)) + 1;
		new String:pw[pwstrlen];
		for (new x = 0; x < pwstrlen; x++)
		{
			decl String:buff[2];
			IntToString(GetRandomInt(0, 9), buff, sizeof(buff));
			StrCat(pw, pwstrlen, buff);
		}
		SetConVarString(g_Cvar_PW, pw, false, true);
		// This does not seem to work, sad :(
		// if (setToClientsInfo)
		// {
		// 	for (new i = 1; i <= MaxClients; i++)
		// 	{
		// 		if(IsClientInGame(i) && !IsFakeClient(i))
		// 		{
		// 			SetClientInfo(i, "password", pw);
		// 		}
		// 	}
		// }
	}
}

bool:STVBotIsPresent()
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if(IsClientSourceTV(i))
		{
			return true;
		}
	}
	return false;
}

GetStvIndex()
{
	for (new i = 1; i <= MaxClients; i++)
	{
		if(IsClientSourceTV(i))
		{
			return i;
		}
	}
	return -1;
}